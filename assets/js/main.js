let slideIndex = 0;
let isDragging = false;
let startX = 0;
let currentTranslate = 0;
let previousTranslate = 0;

function showSlide(index) {
    const slides = document.querySelector('.slides');
    if (!slides) return;

    const slideWidth = document.querySelector('.slide').offsetWidth;
    const totalSlides = document.querySelectorAll('.slide').length;

    // Tự động tính số lượng slide hiển thị dựa trên kích thước màn hình
    const visibleSlides = Math.min(11, Math.floor(slides.offsetWidth / slideWidth));

    // Giới hạn chỉ số slide
    if (index >= totalSlides - visibleSlides + 1) {
        slideIndex = 0;
    } else if (index < 0) {
        slideIndex = totalSlides - visibleSlides;
    } else {
        slideIndex = index;
    }

    // Cập nhật vị trí của slider
    currentTranslate = -slideIndex * slideWidth;
    slides.style.transform = `translateX(${currentTranslate}px)`;
    slides.style.transition = "transform 0.3s ease";
}

function nextSlide() {
    showSlide(slideIndex + 1);
}

function prevSlide() {
    showSlide(slideIndex - 1);
}

function touchStart(event) {
    const slides = document.querySelector('.slides');
    if (!slides) return;

    isDragging = true;
    startX = event.type === "touchstart" ? event.touches[0].clientX : event.clientX;
    slides.style.transition = "none"; // Tắt chuyển động khi kéo
}

function touchMove(event) {
    if (!isDragging) return;

    const slides = document.querySelector('.slides');
    if (!slides) return;

    const currentX = event.type === "touchmove" ? event.touches[0].clientX : event.clientX;
    const deltaX = currentX - startX;

    currentTranslate = previousTranslate + deltaX;
    slides.style.transform = `translateX(${currentTranslate}px)`;
}

function touchEnd() {
    const slides = document.querySelector('.slides');
    if (!slides) return;

    isDragging = false;

    // Tính toán lại chỉ số slide dựa trên vị trí kéo
    const slideWidth = document.querySelector('.slide').offsetWidth;
    slideIndex = Math.round(-currentTranslate / slideWidth);

    // Cập nhật vị trí
    showSlide(slideIndex);
    previousTranslate = currentTranslate;
}

document.addEventListener("DOMContentLoaded", () => {
    const slides = document.querySelector('.slides');
    if (!slides) return;

    showSlide(slideIndex);

    const nextButton = document.querySelector('.next-slide');
    const prevButton = document.querySelector('.prev-slide');

    if (nextButton) nextButton.addEventListener("click", nextSlide);
    if (prevButton) prevButton.addEventListener("click", prevSlide);

    // Thêm sự kiện kéo tay
    slides.addEventListener("mousedown", touchStart);
    slides.addEventListener("mousemove", touchMove);
    slides.addEventListener("mouseup", touchEnd);
    slides.addEventListener("mouseleave", touchEnd);

    slides.addEventListener("touchstart", touchStart, { passive: true });
    slides.addEventListener("touchmove", touchMove, { passive: true });
    slides.addEventListener("touchend", touchEnd);
});
